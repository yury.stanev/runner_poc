const Docker = require('dockerode');
const interface = require('./interface');
const shell = require('shelljs');
const fs = require('fs-extra');
const docker = new Docker() // instantiates `dockerode`

function runExec(container) {

    var options = {
        Cmd: ['bash', '-c', './script.sh'],
        AttachStdout: true,
        AttachStderr: true
    }; 

    container.exec(options, function (err, exec) {
        if (err) return;
        exec.start(function (err, stream) {
            if (err) return;
            container.modem.demuxStream(stream, process.stdout, process.stderr);
        });
    });
}

function createContainer() {
    docker.createContainer({
        Image: 'ubuntu',
        Tty: true,
        Cmd: ['/bin/bash']
    }, function (err, container) {
        if (err) return;

        container.start({}, function (err, data) {
            const stream = fs.createWriteStream('./containerID.json');
            stream.write(JSON.stringify({
                containerID: container.id
            }));

            shell.exec(`docker cp ./script.sh ${container.id}:/`) // copy the file to the container

            runExec(container);
        });
    });
}

docker.run()

async function checkForRunningContainer() {
    let file;

    try {
        file = require("./containerID.json")
    } catch (e) {

    }

    if (file && file.containerID) {
        const container = interface.getContainer(file.containerID)
        let state = await interface.getState(container)
        // console.log(state)

        if (state) {
            // console.log('here')
            interface.containerAttach(container);
        } else {
            createContainer()
        }
    } else {
        createContainer()
    }
}

checkForRunningContainer()