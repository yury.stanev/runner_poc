const Docker = require('dockerode');
const docker = new Docker() // instantiates `dockerode`

const {
    promisify
} = require('util');

exports.getContainer = (id) => docker.getContainer(id)

exports.getState = async (container) => {
    return await new Promise(function (resolve, reject) {
        container.inspect({}, function (err, data) {
            if (err) return;
            const state = data.State.Running;
            // console.log(typeof (state), state)
            resolve(state);
        })
    })
}

exports.containerAttach = (container) => {
    container.attach({
        stream: true,
        stdout: true,
        stderr: true
    }, function (err, stream) {
        stream.pipe(process.stdout);
    })
}