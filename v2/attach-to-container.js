const docker = new(require('dockerode'));
const container = docker.getContainer(require('./container.json').id);

async function getState() {
    return await new Promise(function (resolve, reject) {
        container.inspect({}, function (err, data) {
            if (err) return;
            const state = data.State.Running;
            // console.log(typeof (state), state)
            resolve(state);
        })
    })
}

if (getState()) {
    container.attach({
        stream: true,
        stderr: true,
        stdout: true
    }, (err, stream) => {
        if (err) console.log(`The container has ${container.id} been removed:\n\n${err}`);
        console.log(`Reattached to container: ${container.id}`);

        stream.on('data', (data) => console.log('-- STREAM --\n', data.toString()));
        stream.on('end', () => {
            console.log('\n-- STREAM END --')
        });
    });
} else {
    console.log(`Container ${container.id} has exited`)
}


//
// ─── CLEANUP ────────────────────────────────────────────────────────────────────
//

process.on('SIGTERM', () => {
    console.log('Killing Container', container.id);

    container.remove({
        force: true,
    }, (err) => {
        if (err) throw err;
        process.exit(0);
    });
});

process.on('SIGINT -> Crtl + C', () => {
    console.log('Killing Container', container.id);

    container.remove({
        force: true,
    }, (err) => {
        if (err) throw err;
        process.exit(0);
    });
});