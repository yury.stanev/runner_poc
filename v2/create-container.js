const docker = new(require('dockerode'));
const fs = require('fs-extra');
const path = require('path');

docker.run('node', ['./home/run-in-container.sh'], false, {
        Hostconfig: {
            Binds: [`${__dirname}:/home`]
        }
    }, {}, (err, data, container) => {
        if (err) throw err;
        console.log(data.StatusCode)
        console.log(container)
    })
    .on('container', (container) => {
        setTimeout(() => {
            fs.writeFileSync(path.join(__dirname, 'container.json'), JSON.stringify({
                id: container.id
            }));
            process.exit(0);
        }, 5000)
    })
    .on('stream', (stream) => {
        stream.on('data', data => console.log('-- STREAM --\n', data.toString()));
    })
    .on('data', (data) => {
        console.log('data', data);
    });
