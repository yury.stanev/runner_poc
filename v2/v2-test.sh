#!/bin/bash

echo ""
echo "---> docker ps\n"
echo ""

docker ps
sleep 2

echo ""
echo "---> node create-poc.js\n"
echo ""

node create-container.js
sleep 2

echo ""
echo "---> docker ps\n"
echo ""

docker ps
sleep 2

echo ""
echo "---> node attach-poc.js\n"
echo ""

node attach-to-container.js